var AlgebraLatex = require('algebra-latex');

const evaluatex = require("evaluatex");

const variable_value = {
    e: Math.exp(1),
    a: 1,
    b: 500,
    c: 10,
    x: 10,
    y: 10,
    PI: Math.PI
};


//define latex input//
var input_string = "\(4+5\)"
var input_string2 = "\(sqrt(3^2 + 4^2) + log(PI) / log(SQRT2)\)"
var input_string3 = "\(\dfrac{4e^{-7x}}{-8*2}\)"
var input_string4 = "\(e+2\)"
var input_string5 = "\(\sqrt{b^2-4ac} \over 2a\)"
var input_string6 = "\(sqrt(b^2 + 4ac)/2a\)"
var input_string7 = "\(4*a/5*b*c\)"
var input_string8 = "\(sqrt(3^2a + 4b/2ac)/(2ab)\)"
var input_string10 = "\(\sin{x}\)"
var input_string11 = "\(\sin{x+2\Pi}\)"


function Replace_latex_symbol(str) {
    let result = "";
    console.log("string for replacement", str, "\n");

    result = str.replace(new RegExp(`dfrac`), '\\frac');
    //replace "dfrac" to "frac"

    result = str.replace(new RegExp(`sqrt`), '\sqrt');
    //replace "sqrt" to "\sqrt"

    result = result.replace(new RegExp(`over`), '/');
    // replace "over" to "/"" math expression

    result = result.replace(new RegExp(`cdot`), '*');
    // replace "dot" as "*"" in maths calculation

    result = result.replace(new RegExp(`Pi`), 'PI');
    // replace "Pi" as "PI"" in maths calculation

    result = result.replace(/\s/g, "");
    // remove all whitespace

    let numeric_character_group = result.match(/(\d\w{1,})/g);
    //seperate into array objects {[4a], [4abc]}

    console.log("numeric_character_group", numeric_character_group, "\n");

    if (numeric_character_group != null) {
        for (i = 0; i < numeric_character_group.length; i++) {
            var newObj = "";
            let obj = numeric_character_group[i];
            console.log("obj length", obj.length);
            if (!(obj.includes("e")) && !(obj.includes("PI"))) {
                for (j = 0; j < numeric_character_group[i].length; j++) {
                    newObj = newObj.concat(obj[j], "*");
                    console.log(newObj);
                }
                newObj = newObj.substring(0, newObj.length - 1);
                newObj = "(".concat(newObj, ")");
                console.log(newObj);
                result = result.replace(numeric_character_group[i], newObj);
            }
        }
    }

    console.log("Replacement finish", result, "\n");
    return result;
}

fn = evaluatex(Replace_latex_symbol(input_string5), variable_value, {
    latex: true
});

result = fn();

console.log("\n");
console.log(result);