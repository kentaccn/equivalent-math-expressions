Equivalent-Math-Expressions
=========

In maths, a+b+c = b+a+c = a+c+b = ... however, in Latex form, it is hard to and also unable to distinguish these math expressions are equivant or not. 

Equivalent-Math-Expressions is a node JS program to reformat the latex syntax of [MyScript](https://www.myscript.com/) to latex and javascript form, then use [**Evaluatex**](https://github.com/arthanzel/evaluatex) to perform calculation by giving value to maths variables.

**Evaluatex** is a parser that reads and evaluates LaTeX and ASCII math. Use in Node, Angular, or with vanilla Javascript on both clients and servers.

**Evaluatex** can safely resolve math without relying Javascript's native `eval()` function. It can also support custom variables and functions, so you can pass in arbitrary values without having to hard-code the math.

> **Evaluatex** is reasonably stable. It has been tested for common patterns and use cases, but when provoked it may do weird and unexpected things such as eating your cat.

Installation
-----
```bash
npm install evaluatex
# or
yarn install/add evaluatex
```

```javascript
const evaluatex = require("evaluatex");
// or
import evaluatex from "evaluatex";
```

Building
--------
Get [yarn](https://yarnpkg.com/en/) and do `yarn install`.

`yarn build` transpiles ES6 sources to `dist/`.

`yarn test` runs tests in the `test/` directory.

Features and usage
------------------
A complete manual is available on the [project page](https://arthanzel.github.io/evaluatex).